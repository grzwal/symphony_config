#These conf will only take work on Linux 64
file|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/pmrgui.xml.advanced|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/pmrgui.xml|Y
file|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/mapreducegui.xml.advanced|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/mapreducegui.xml|Y
removefile|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/pmrgui.xml.advanced||Y
removefile|@EGO_CONFDIR@/../../gui/conf/Catalina/localhost/mapreducegui.xml.advanced||Y

#MapReduce part for Linux 64
file|@INSTALLDIR@/.install/sym_advanced_edition/ConsumerTrees.xml|@EGO_CONFDIR@/../../kernel/conf/ConsumerTrees.xml|Y
file|@INSTALLDIR@/.install/sym_advanced_edition/MapReduce7.1.xml|@EGO_CONFDIR@/../../soam/profiles/enabled/MapReduce7.1.xml|Y

#MapReduce perf part for Linux 64
file|@INSTALLDIR@/.install/sym_advanced_edition/mapreducemetrcis.xml|@EGO_CONFDIR@/../../perf/conf/dataloader/mapreducemetrcis.xml|Y
file|@INSTALLDIR@/.install/sym_advanced_edition/pmrresourcemetrics.xml|@EGO_CONFDIR@/../../perf/conf/dataloader/pmrresourcemetrics.xml|Y
file|@INSTALLDIR@/.install/sym_advanced_edition/symsessionloaderhour.xml|@EGO_CONFDIR@/../../perf/conf/dataloader/symsessionloaderhour.xml|Y
file|@INSTALLDIR@/.install/sym_advanced_edition/plc_pmr.xml|@EGO_CONFDIR@/../../perf/conf/plc/plc_pmr.xml|Y
file|@INSTALLDIR@/.install/sym_advanced_edition/purger_pmr.xml|@EGO_CONFDIR@/../../perf/conf/purger/purger_pmr.xml|Y


