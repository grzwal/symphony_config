# ego.conf
addline|@EGO_CONFDIR@/ego.conf|EGO_GPU_ENABLED=|EGO_GPU_ENABLED=Y
addline|@EGO_CONFDIR@/ego.conf|EGO_RUSAGE_DYNAMIC_METRICS=|EGO_RUSAGE_DYNAMIC_METRICS=Y
#ego.shared
replacestring|@EGO_CONFDIR@/ego.shared|End Resource|gpushared      Numeric 15 Y (Number of GPUs in Normal mode)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|ngpus   Numeric 15 Y (Number of GPUs)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuexclusive_thread   Numeric 15 Y (Number of GPUs in Exclusive Thread mode)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuprohibited  Numeric 15 Y (Number of GPUs in Prohibited mode)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuexclusive_process   Numeric 15 Y (Number of GPUs in Exclusive Process mode)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpudriverversion String 15 () (driver version)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpusdkversion Numeric 15 Y (driver version)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucap1_0       Numeric 15 Y (number of devices with specific capability)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucap1_1       Numeric 15 Y (number of devices with specific capability)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucap1_2       Numeric 15 Y (number of devices with specific capability)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucap1_3       Numeric 15 Y (number of devices with specific capability)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucap2_plus   Numeric 15 Y (number of devices with specific capability)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucapver0     Numeric  15 Y (the capability of the device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumode0       Numeric  15 Y (mode for GPU device 0; 0 -Normal, 1 -Exclusive, 2 -Prohibited)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gputemp0       Numeric  15 Y (temperature for GPU device 0)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuecc0        Numeric  15 Y (number of ECC errors for GPU device 0)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuut0         Numeric  15 Y (gpu utilization)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumem0        Numeric  15 Y (current memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumaxmem0     Numeric  15 Y (maximum memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuncores0     Numeric  15 Y (number of cores per device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucapver1     Numeric  15 Y (the capability of the device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumode1       Numeric  15 Y (mode for GPU device 1; 0 -Normal, 1 -Exclusive, 2 -Prohibited)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gputemp1       Numeric  15 Y (temperature for GPU device 1)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuecc1        Numeric  15 Y (number of ECC errors for GPU device 1)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuut1         Numeric  15 Y (gpu utilization)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumem1        Numeric  15 Y (current memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumaxmem1     Numeric  15 Y (maximum memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuncores1     Numeric  15 Y (number of cores per device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucapver2     Numeric  15 Y (the capability of the device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumode2       Numeric  15 Y (mode for GPU device 2; 0 -Normal, 1 -Exclusive, 2 -Prohibited)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gputemp2       Numeric  15 Y (temperature for GPU device 2)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuecc2        Numeric  15 Y (number of ECC errors for GPU device 2)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuut2         Numeric  15 Y (gpu utilization)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumem2        Numeric  15 Y (current memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumaxmem2     Numeric  15 Y (maximum memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuncores2     Numeric  15 Y (number of cores per device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpucapver3     Numeric  15 Y (the capability of the device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumode3       Numeric  15 Y (mode for GPU device 3; 0 -Normal, 1 -Exclusive, 2 -Prohibited)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gputemp3       Numeric  15 Y (temperature for GPU device 3)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuecc3        Numeric  15 Y (number of ECC errors for GPU device 3)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuut3         Numeric  15 Y (gpu utilization)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumem3        Numeric  15 Y (current memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpumaxmem3     Numeric  15 Y (maximum memory available)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|gpuncores3     Numeric  15 Y (number of cores per device)
addline|@EGO_CONFDIR@/ego.shared|abcdefgh|End Resource
#ego.cluster.<clsutername>
replacestring|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|End ResourceMap|gpushared     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|ngpus [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuexclusive_thread [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuexclusive_process [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuprohibited [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpudriverversion [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpusdkversion [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucap1_0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucap1_1     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucap1_2     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucap1_3     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucap2_plus [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucapver0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumode0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gputemp0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuecc0  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuut0      [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumem0  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumaxmem0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuncores0     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucapver1    [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumode1     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gputemp1     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuecc1  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuut1      [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumem1  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumaxmem1     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuncores1     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucapver2    [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumode2     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gputemp2     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuecc2  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuut2      [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumem2  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumaxmem2     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuncores2     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpucapver3    [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumode3     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gputemp3     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuecc3  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuut3      [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumem3  [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpumaxmem3     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|gpuncores3     [default]
addline|@EGO_CONFDIR@/ego.cluster.@CLUSTERNAME@|abcdefgh|End ResourceMap
