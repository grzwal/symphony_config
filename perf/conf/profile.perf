#!/bin/sh
#--------------------------------------------------------------------------
# This code gets the binary type when setting LSF/EGO AND PERF environment.
#--------------------------------------------------------------------------

if [ -z "${EGO_TOP}" -o -z "${EGO_CONFDIR}" ]; then
	echo "To run this file, first set your Platform Symphony environment using profile.platform. Do not run this file separately."
	return 1
fi

if [ -z "${EGO_TOP}" ]; then
	EGO_TOP=`grep '^[ 	]*EGO_TOP' ${EGO_CONFDIR}/ego.conf | sed -e 's/^.*=//g'`
fi

# Clear environment variables or not
if [ "$1" != "true" ]; then
	PERF_TOP=""
	PERF_CONFDIR=""
	PERF_WORKDIR=""
	PERF_DATADIR=""
	PERF_LOGDIR=""
fi

if [ -n "${LSF_ENVDIR}" ]; then
	if [ -z "${PERF_TOP}" ] ; then
		PERF_TOP=`grep '^[ 	]*PERF_TOP' ${LSF_ENVDIR}/lsf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
	fi
    if [ -z "${PERF_CONFDIR}" ] ; then
    	PERF_CONFDIR=`grep '^[ 	]*PERF_CONFDIR' ${LSF_ENVDIR}/lsf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
	fi
	if [ -z "${PERF_WORKDIR}" ] ; then
    	PERF_WORKDIR=`grep '^[ 	]*PERF_WORKDIR' ${LSF_ENVDIR}/lsf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
	fi
	if [ -z "${PERF_DATADIR}" ] ; then
    	PERF_DATADIR=`grep '^[ 	]*PERF_DATADIR' ${LSF_ENVDIR}/lsf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
	fi
	if [ -z "${PERF_LOGDIR}" ] ; then
    	PERF_LOGDIR=`grep '^[ 	]*PERF_LOGDIR' ${LSF_ENVDIR}/lsf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
	fi
fi

# set PERF_CONFDIR
if [ -z "${PERF_CONFDIR}" ] ; then
	PERF_CONFDIR=${EGO_CONFDIR}/../../perf/conf
fi

# set PERF_TOP
if [ -z "${PERF_TOP}" ] ; then
	PERF_TOP=`grep '^[ 	]*PERF_TOP' ${PERF_CONFDIR}/perf.conf | sed -e 's/^.*=//g' | sed 's|${EGO_TOP}|{{{EGO_TOP}}}|g' | sed "s?{{{EGO_TOP}}}?$EGO_TOP?g"`
fi
if [ -z "${PERF_TOP}" ] ; then
	PERF_TOP=${EGO_TOP}/perf
fi

# set PERF_WORKDIR
if [ -z "${PERF_WORKDIR}" ] ; then
	PERF_WORKDIR=`grep '^[ 	]*PERF_WORKDIR' ${PERF_CONFDIR}/perf.conf | sed -e 's/^.*=//g' | sed 's|${EGO_CONFDIR}|{{{EGO_CONFDIR}}}|g' | sed "s?{{{EGO_CONFDIR}}}?$EGO_CONFDIR?g"`
fi
if [ -z "${PERF_WORKDIR}" ] ; then
	PERF_WORKDIR=${EGO_CONFDIR}/../../perf/work
fi

# Set PERF_DATADIR
if [ -z "${PERF_DATADIR}" ] ; then
	PERF_DATADIR=`grep '^[ 	]*PERF_DATADIR' ${PERF_CONFDIR}/perf.conf | sed -e 's/^.*=//g' | sed 's|${EGO_TOP}|{{{EGO_TOP}}}|g' | sed "s?{{{EGO_TOP}}}?$EGO_TOP?g"`
fi
if [ -z "${PERF_DATADIR}" ] ; then
	PERF_DATADIR=${PERF_TOP}/data
fi

# Set PERF_LOGDIR
if [ -z "${PERF_LOGDIR}" ] ; then
	PERF_LOGDIR=`grep '^[ 	]*PERF_LOGDIR' ${PERF_CONFDIR}/perf.conf | sed -e 's/^.*=//g' | sed 's|${EGO_TOP}|{{{EGO_TOP}}}|g' | sed "s?{{{EGO_TOP}}}?$EGO_TOP?g"`
fi
if [ -z "${PERF_LOGDIR}" ] ; then
	PERF_LOGDIR=${PERF_TOP}/logs
fi

# Get perf version match #.# pattern.
#if [ -z "${PERF_VERSION}" ] ; then
	PERF_VERSION=`grep '^[ 	]*PERF_VERSION' ${PERF_CONFDIR}/perf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
#	PERF_VERSION=`echo ${PERF_VERSION} | sed "s/\(.*\)\.\(.*\)\.\(.*\)/\1\.\2/g"`
#fi

if [ -z "${EGO_VERSION}" ] ; then
	EGO_VERSION=`grep '^[ 	]*EGO_VERSION' ${PERF_CONFDIR}/perf.conf | sed -e "s/^.*=//g" | sed -e "s/\"//g"`
fi

#Get JAVA HOME from perf.conf
#JAVA_HOME=`grep '^[ |	]*JAVA_HOME' ${PERF_CONFDIR}/perf.conf | sed -e 's/^.*=//g'`
JAVA_HOME=""
PERF_ENV="-DPERF_TOP=${PERF_TOP} -DPERF_CONFDIR=${PERF_CONFDIR} -DPERF_WORKDIR=${PERF_WORKDIR} -DPERF_LOGDIR=${PERF_LOGDIR} -DPERF_DATADIR=${PERF_DATADIR}"

#-----------------------------------
# Name: is_cpuset_host.
# Synopsis: is_cpuset_host
# Environment Variables:
# Description:
#       It checkes whether or not the current host is a cpuset host
#       Check if libcpuset.so exist or not
#       /usr/lib/libcpuset.so
#       /lib32/libcpuset.so
#       /lib64/libcpuset.so
# Return Value:
#       0 cpuset host; 1 non-cpuset host
#-----------------------------------
is_cpuset_host ()
{
    if [ -f  "/usr/lib/libcpuset.so" -o -f "/lib32/libcpuset.so" -o -f "/lib64/libcpuset.so" ]; then
# cpuset host
       return 0
    fi
# Non-cpuset hosts
     return 1
} # is_cpuset_host


_CUR_PATH_ENV="$PATH"
PATH=/usr/bin:/bin:/usr/local/bin:/local/bin:/sbin:/usr/sbin:/usr/ucb:/usr/sbin:/usr/bsd:${PATH}

# handle difference between system V and BSD echo
# To echo "foo" with no newline, do
# echo $enf "foo" $enl

if [ "`echo -n`" = "-n" ] ; then
    enf=
    enl="\c"
else
    enf=-n
    enl=
fi
export enf enl

LSNULFILE=/dev/null

# Find a version of awk we can trust
AWK=""
for tmp in nawk  /usr/toolbox/nawk gawk awk 
do
#   This is the real test, for functions & tolower
#    if foo=`(echo FOO | $tmp 'function tl(str) { return tolower(str) } { print tl($1) }') 2>$LSNULFILE` \
#       && test "$foo" = "foo"
#   A simpler test, just for executability
    if ($tmp 'BEGIN{ } { }') < $LSNULFILE > $LSNULFILE 2>&1  
    then
	AWK=$tmp
        break
    fi
done

if test "$AWK" = ""
then
    echo "Cannot find a correct version of awk."
    echo "Exiting ... "
fi
export AWK

uname_val=`uname -a`
BINARY_TYPE="fail"
BINARY_TYPE_PRIMARY=""
export BINARY_TYPE
field1=`echo $uname_val |$AWK '{print $1}'`
case "$field1" in
         UNIX_System_V)
             BINARY_TYPE="uxp"
	  ;;
	 OSF1)
	     version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
	     if [ $version = "V5" ] ; then
	        BINARY_TYPE="alpha5"
	     else
	         if [ $version = "T5" ] ; then
	            BINARY_TYPE="alpha5"
	         else
	             BINARY_TYPE="alpha"
	         fi
	     fi
	     if [ "$BINARY_TYPE" = "alpha5" ]; then
	     # check for rms
	     # Kite#31479
	         if [ ! -z "`/usr/sbin/rcmgr get SC_MS`" ]; then
	             BINARY_TYPE="alpha5-rms"
                 fi
	      fi
	  ;;
    SunOS)
        version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
        minorver=`echo $uname_val | $AWK '{split($3, a, "."); print a[2]}'`
        machine=`echo $uname_val | $AWK '{print $5}'`
        if [ $version = "4" ]; then
            BINARY_TYPE="sunos4"
        else
            if [ "$machine" = "i86pc" ]; then
                BIT64=`/usr/bin/isainfo -vk | grep -c '64.bit' 2> /dev/null`
                if [ "$BIT64" -eq "0" ] ; then
                    if [ "$minorver" = "7" -o "$minorver" = "8" -o "$minorver" = "9" -o "$minorver" = "10" ] ; then
                        if [ "$minorver" -ge "10" ]; then
	                   BINARY_TYPE="x86-sol10"
			else  
			   BINARY_TYPE="x86-sol7"
			fi    
		    else          
		        BINARY_TYPE="x86-sol2"
		    fi            
		    else          
		      if [ "$minorver" -ge "10" ] ; then
		         BINARY_TYPE="x86-64-sol10"
		    fi        
		fi        
	    else
                BINARY_TYPE="sparc-sol2"
                if [ "$minorver" -ge "7" -a "$minorver" -le "9" ]; then
                    BIT64=`/usr/bin/isainfo -vk | grep -c '64.bit' 2> /dev/null`
                    if [ "$BIT64" -eq "0" ]; then
                        BINARY_TYPE="sparc-sol7-32"
                    else
                        BINARY_TYPE="sparc-sol7-64"
                    fi
                elif [ "$minorver" -ge "10" ]; then
                    BINARY_TYPE="sparc-sol10-64"
                fi
            fi
        fi
        ;;
   HI-UX)
           BINARY_TYPE="hppa"
           ;;
   HP-UX)
        # spp need to run uname -C to find the version
        uname -C 2>$LSNULFILE
        if [ $? = 1 ]; then
            version=`echo $uname_val | $AWK '{split($3, a, "."); print a[2]}'`
            if [ $version = "09" ] ; then
                BINARY_TYPE="hppa9"
            elif [ $version = "10" ] ; then
                subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[3]}'`
                if [ $subver = "10" ]; then
                    BINARY_TYPE="hppa10"
                elif [ $subver = "20" ]; then
                    BINARY_TYPE="hppa10.20"
                fi
            elif [ $version = "11" ] ; then
                machine=`echo $uname_val | $AWK '{print $5}'`
                subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[3]}'`
                if [ "$machine" = "ia64" ]; then
                    BINARY_TYPE="hppa11-64"
                    #
                    # The order in BINARY_TYPE_PRIMARY matters
                    # the order determines how profile.ego/cshrc.ego
                    # sets the EGO_BINDIR. The correct order should be
                    # from high version of OS to low version of OS.
                    #
                    BINARY_TYPE_PRIMARY="hpuxia64 hppa11i-64 hppa11-64"
                else
                    kernel=`/usr/bin/getconf KERNEL_BITS`
                  if [ "$subver" = "00" ]; then
                        if [ "$kernel" = "32" ] ; then
                            BINARY_TYPE="hppa11-32"
                        else
                            BINARY_TYPE="hppa11-64"
                        fi
                   else
                        if [ "$kernel" = "32" ] ; then
                            BINARY_TYPE="hppa11-32"
                            BINARY_TYPE_PRIMARY="hppa11i-32 hppa11-32"
                            # reset to standard EGO binary type
                            for ARCH in hppa11i-32 hppa11-32
                            do
                                if [ -f $EGO_TOP/$EGO_VERSION/$ARCH/etc/vemkd ]; then
                                    BINARY_TYPE="$ARCH"
                                    break;
                                fi
                            done
                        else
                            BINARY_TYPE="hppa11-64"
                            BINARY_TYPE_PRIMARY="hppa11i-64 hppa11-64"
                            # reset to standard EGO binary type
                            for ARCH in hppa11i-64 hppa11-64
                            do
                                if [ -f $EGO_TOP/$EGO_VERSION/$ARCH/etc/vemkd ]; then
                                    BINARY_TYPE="$ARCH"
                                    break;
                                fi
                            done
                        fi

                    fi

                fi
            else
                echo "Cannot figure out the HP version."
		exit 1
            fi
         else
            version=`uname -C -a |  $AWK '{split($3, a, "."); print a[1]}'`
            if [ $version = "5" ]; then
                BINARY_TYPE="spp"
            elif  [ $version = "4" ]; then
                BINARY_TYPE="spp4"
             fi
         fi
        ;;
    AIX)
        version=`echo $uname_val | $AWK '{print $4}'`
        release=`echo $uname_val | $AWK '{print $3}'`
        if [ $version = "4" -a $release -ge 1 ]; then
           if [ $release -lt 2 ]; then
                BINARY_TYPE="aix4"
           elif [ $release -ge 2 ]; then
                 BINARY_TYPE="aix4.2"
           fi
        elif [ $version = "5" -a $release -ge 1 ]; then
            #On AIX, only in 64bit mode, you can see shlap64 process
            #check shlap64 process is safer than check kernel_bit
            if [ `ps -e -o "comm" |grep shlap64` ]; then
               BINARY_TYPE="aix5-64"
            else
	        BINARY_TYPE="aix5-32"
            fi
	elif [ $version = "3" ]; then
            BINARY_TYPE="aix3"
        elif [ $version = "6" ]; then
           if [ `ps -e -o "comm" |grep shlap64` ]; then
             BINARY_TYPE="aix5-64"
           else
             BINARY_TYPE="aix5-32"
           fi
  	fi
	;;
    Linux)
        version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
        #get glibc version
        for glibc in `ldconfig -p | grep libc.so.6 | sed 's/.*=>//'`
        do
            if [ -x "$glibc" ] ; then
                $glibc >/dev/null 2>&1
                if [ "$?" != "0" ] ; then
                    continue
                fi
                _libcver=`$glibc 2>/dev/null | grep "GNU C Library" | awk '{print substr($0,match($0,/version/))}' | awk '{print $2}' | awk -F. '{print $2}'| sed 's/,//'`
            fi
            if [ "$_libcver" != "" ] ; then
                break
            fi
        done
        if [ "$_libcver" = "" ] ; then
            echo "Cannot figure out the GLibc version."
            exit 1
        fi

        if [ $version = "1" ]; then
	    BINARY_TYPE="linux"
        elif [  $version = "2" ]; then
            subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]"."a[2]}'`
            libcver=`ls /lib/libc-* 2> /dev/null`
            libcver=`echo $libcver | $AWK '{split($1, a, "."); print a[3]}'`

            if [ $subver = "2.0" ]; then
	        BINARY_TYPE="linux2"
                machine=`echo $uname_val | $AWK '{print $11}'`
                if [ "$machine" = "alpha" ]; then
                    BINARY_TYPE="linux2-alpha"
                else
                    BINARY_TYPE="linux2-intel"
                fi
            elif [  $subver = "2.2" ]; then
                BINARY_TYPE="linux2.2"

                smp=`echo $uname_val | $AWK '{print $5}'`
                if [ "$smp" = "SMP" ]; then
                    machine=`echo $uname_val | $AWK '{print $12}'`
                else
                    machine=`echo $uname_val | $AWK '{print $11}'`
                fi

                if [ "$machine" = "alpha" ]; then
                    BINARY_TYPE="linux2.2-glibc2.1-alpha"
                elif [ "$machine" = "sparc" -o "$machine" =  "sparc64" ]; then
		    BINARY_TYPE="linux2.2-glibc2.1-sparc"
                elif [ "$machine" = "ppc" ]; then
		    BINARY_TYPE="linux2.2-glibc2.1-powerpc"
		else
                    BINARY_TYPE="linux2.2-glibc2.1-x86"
                fi

            elif [  $subver = "2.4" ]; then
                BINARY_TYPE="linux2.4"

# ia64 is the family of 64-bit CPUs from Intel. We shouldn't need a new
# distribution for each processor
                smp=`echo $uname_val | $AWK '{print $5}'`
                if [ "$smp" = "SMP" ]; then
                    machine=`echo $uname_val | $AWK '{print $12}'`
                else
                    machine=`echo $uname_val | $AWK '{print $11}'`
                fi

                if [ "$machine" = "ia64" ]; then
	            _libcver=`ls /lib/libc-* 2> /dev/null`
		    _libcver=`echo "$_libcver" | $AWK '{split($1, a, "."); print a[2]}'`
		    if [ "$_libcver" = "1" ]; then
		        BINARY_TYPE="linux2.4-glibc2.1-ia64"
	            elif [ "$_libcver" = "2" ]; then
                        BINARY_TYPE="linux2.4-glibc2.2-ia64"
                    else
		        BINARY_TYPE="linux2.4-glibc2.3-ia64"
		    fi
                elif [ "$machine" = "ppc64" ]; then
                    BINARY_TYPE="linux2.4-glibc2.2-ppc64"
                elif [ "$machine" = "s390" ]; then
                    BINARY_TYPE="linux2.4-glibc2.2-s390-32"
                elif [ "$machine" = "s390x" ]; then
                    BINARY_TYPE="linux2.4-glibc2.2-s390x-64"
	        elif [ "$machine" = "armv5l" ]; then
                    BINARY_TYPE="linux2.4-glibc2.2-armv5l"
                elif [ "$machine" = "x86_64" ]; then
                    _cputype=`cat /proc/cpuinfo | grep -i vendor | $AWK '{print $3}' | uniq`
                    if [ "$_libcver" = "1" ]; then
                        if [ "$_cputype" = "GenuineIntel" ]; then
                            BINARY_TYPE="linux2.4-glibc2.1-x86"
                            #BINARY_TYPE="linux2.4-glibc2.1-ia32e"
                        else
                            BINARY_TYPE="linux2.4-glibc2.1-x86"
                            #BINARY_TYPE="linux2.4-glibc2.1-amd64"
                        fi
                    elif [ "$_libcver" = "2" ]; then
                        if [ "$_cputype" = "GenuineIntel" ]; then
                            #BINARY_TYPE="linux2.4-glibc2.2-x86"
                            BINARY_TYPE="linux2.4-glibc2.2-x86_64"
                        else
                            #BINARY_TYPE="linux2.4-glibc2.2-x86"
                            BINARY_TYPE="linux2.4-glibc2.2-x86_64"
                        fi
                    elif [ "$_libcver" = "3" ]; then
                        if [ "$_cputype" = "GenuineIntel" ]; then
                            #BINARY_TYPE="linux2.4-glibc2.3-x86"
                            BINARY_TYPE="linux2.4-glibc2.2-x86_64"
                        else
                            #BINARY_TYPE="linux2.4-glibc2.3-x86"
                            BINARY_TYPE="linux2.4-glibc2.2-x86_64"
                        fi
                    fi
                elif [ "$machine" = "alpha" ]; then
                    BINARY_TYPE="linux2.4-glibc2.2-alpha"
                else
                    BINARY_TYPE="fail"
                    if [ "$_libcver" = "1" ]; then
                        BINARY_TYPE="linux2.4-glibc2.1-x86"
                    elif [ "$_libcver" = "2" ]; then
                        BINARY_TYPE="linux2.4-glibc2.2-x86"
                    elif [ "$_libcver" = "3" ]; then
                        BINARY_TYPE="linux2.4-glibc2.3-x86"
                    fi
                fi
            elif [  $subver = "2.6" ]; then
                BINARY_TYPE="fail"
                machine=`uname -m`

                case "$machine" in
		    ppc64)
		        if [ "$_libcver" = "3" ]; then
		            BINARY_TYPE="linux2.6-glibc2.3-ppc64"
		            BINARY_TYPE_PRIMARY="linux2.6-glibc2.3-ppc64-bluegene linux2.6-glibc2.3-ppc64"
		        elif [ "$_libcver" = "4" ]; then
		            BINARY_TYPE="linux2.6-glibc2.3-ppc64"
		        elif [ "$_libcver" -ge "5" ]; then
		            BINARY_TYPE="linux2.6-glibc2.5-ppc64"
		        fi
		        ;;
                    ia64)
                        if [ "$_libcver" = "1" ]; then
                            BINARY_TYPE="linux2.6-glibc2.1-ia64"
                        elif [ "$_libcver" = "2" ]; then
                            BINARY_TYPE="linux2.6-glibc2.2-ia64"
                        elif [ "$_libcver" = "3" ]; then
                            BINARY_TYPE="linux2.6-glibc2.3-ia64"
			     is_cpuset_host
                             if [ "$?" = "0" ]; then
                                BINARY_TYPE_PRIMARY="linux2.6-glibc2.3-sn-ipf"
                             fi
                        elif [ "$_libcver" = "4" -o "$_libcver" = "5" ]; then
                            BINARY_TYPE="linux2.6-glibc2.3-ia64"
			# regular linux 2.6 glibc 2.4 is not supported without cpusets	
			    BINARY_TYPE_PRIMARY="linux2.6-glibc2.4-sn-ipf"
                        fi
                        ;;
                    x86_64)
                        _cputype=`cat /proc/cpuinfo | grep -i vendor | $AWK '{print $3}' | uniq`
                        if [ "$_libcver" = "1" ]; then
                            if [ "$_cputype" = "GenuineIntel" ]; then
                                BINARY_TYPE="linux2.6-glibc2.1-x86"
                                #BINARY_TYPE="linux2.6-glibc2.1-ia32e"
                            else 
                                BINARY_TYPE="linux2.6-glibc2.1-x86"
                                #BINARY_TYPE="linux2.6-glibc2.1-amd64"
                            fi
                        elif [ "$_libcver" = "2" ]; then
                            if [ "$_cputype" = "GenuineIntel" ]; then
                                #BINARY_TYPE="linux2.6-glibc2.2-x86"
                                BINARY_TYPE="linux2.6-glibc2.2-x86_64"
                            else
                                #BINARY_TYPE="linux2.6-glibc2.2-x86"
                                BINARY_TYPE="linux2.6-glibc2.2-x86_64"
                            fi
                        elif [ "$_libcver" = "3" ]; then
                            if [ "$_cputype" = "GenuineIntel" ]; then
                                #BINARY_TYPE="linux2.6-glibc2.3-x86"
                                BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                            else
                                #BINARY_TYPE="linux2.6-glibc2.3-x86"
                                BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                            fi
                        elif [ "$_libcver" -ge "4" ]; then
                                BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                        fi
                        ;;
                   i[3456]86)
                        if [ "$_libcver" = "1" ]; then
                            BINARY_TYPE="linux2.6-glibc2.1-x86"
                        elif [ "$_libcver" = "2" ]; then
                            BINARY_TYPE="linux2.6-glibc2.2-x86"
                        elif [ "$_libcver" -ge 3 ]; then
                            BINARY_TYPE="linux2.6-glibc2.3-x86"
                        fi
                        ;;
                esac
            fi  #### end of if subver = 2.6 ####
        elif [ $version = "3" ]; then
            subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[2]}'`
            if [ $subver -ge "0" ]; then
                BINARY_TYPE="fail"
                machine=`uname -m`
                case "$machine" in
                    x86_64)
                        if [ "$_libcver" -ge "3" ]; then
                            BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                        fi
                        ;;
                     ppc64)
                        BINARY_TYPE="linux2.6-glibc2.5-ppc64"
                        ;;
                esac
            fi
        fi
        ;;
	 UNIX_SV)
	 BINARY_TYPE="mips-nec"
	 ;;
	 Darwin)
	 BINARY_TYPE="macosx"
	 ;;
	 NEWS-OS)
	 BINARY_TYPE="mips-sony"
	 ;;
	 IRIX*)
	   version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
	   release=`echo $uname_val | $AWK '{split($3, a, "."); print a[2]}'`
	   if [ $version = "6" -a $release -ge "2" ]; then
	      mls=`sysconf MAC`
	      if [ "$mls" = "1" ]
	      then
	         if [ $version = "6" -a $release -eq "5" ]; then
	              modification=`/sbin/uname -R | awk '{split($2, a, "."); print a[3]}' | awk -F'[.a-zA-Z]' '{print $1}'`
	         if [ -z "$modification" ]; then
	              BINARY_TYPE="trix6"
	         else
	           if [ $modification -ge "8" ]; then
	              BINARY_TYPE="trix6.5.24"
	           else
	              BINARY_TYPE="trix6"
	           fi
	        fi
	        else
	              BINARY_TYPE="trix6"
	        fi
	          else
	             if [ $version = "6" -a $release -eq "5" ]; then
	               modification=`/sbin/uname -R | awk '{split($2, a, "."); print a[3]}' | awk -F'[.a-zA-Z]' '{print $1}'`
	                if [ -z "$modification" ]; then
	                    BINARY_TYPE="sgi6.5"
	                else
	                 if [ $modification -ge "8" ]; then
	                    BINARY_TYPE="irix6.5.24"
	                 else
	                    BINARY_TYPE="sgi6.5"
                     fi
		     fi
                    elif [ $version = "6" -a $release -eq "4" ]; then
                        BINARY_TYPE="sgi6"
                 elif [ $version = "6" -a $release -eq "2" ]; then
                    BINARY_TYPE="sgi6"
                    fi
                   fi
	          else
	             BINARY_TYPE="sgi5"
	           fi
        ;;
 SUPER-UX)
	 lastfield=`echo $uname_val | $AWK '{print $NF}'`
	   if [ $lastfield = "SX-3" ]; then
	       BINARY_TYPE="sx3"
	   elif [  $lastfield = "SX-4" ]; then
	       BINARY_TYPE="sx4"
	  elif [  $lastfield = "SX-5" ]; then
	       BINARY_TYPE="sx5"
	  elif [  $lastfield = "SX-6" ]; then
	       BINARY_TYPE="sx6"
	  elif [  $lastfield = "SX-8" ]; then
	       BINARY_TYPE="sx8"
	  fi
	 ;;
   ULTRIX)
        BINARY_TYPE="ultrix"
    ;;
  ConvexOS)
        BINARY_TYPE="convex"
   ;;
    *)
	;;
esac
if [ $BINARY_TYPE = "fail" ]; then
    echo "Cannot get binary type"
fi
if [ "$BINARY_TYPE_PRIMARY" != "" ]; then
    for _ARCH in $BINARY_TYPE_PRIMARY
    do
        if [ -f "$EGO_TOP/$EGO_VERSION/$_ARCH/etc/vemkd" ]; then
            BINARY_TYPE="$_ARCH"
            break
        fi
    done
    export BINARY_TYPE
fi

if [ -z "${JAVA_HOME}" -o "${JAVA_HOME}" = "" ]; then
    case `uname` in
    Linux)
                case `uname -m` in
                ppc64)
                        OS_TYPE=linux-ppc64
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE
                        else
                                JAVA_HOME=$PERF_TOP/../jre/$OS_TYPE
                        fi
                        PERF_LIB_TYPE=linux-ppc64
                        ;;
                x86_64)
                        OS_TYPE=linux-x86_64
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE
                        else
                                JAVA_HOME=$PERF_TOP/../jre/$OS_TYPE
                        fi
                        PERF_LIB_TYPE=linux-x86_64
                        ;;
                ia64)
                        OS_TYPE=linux-ia64
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE
                        else
                                JAVA_HOME=$PERF_TOP/../jre/$OS_TYPE
                        fi
                        PERF_LIB_TYPE=linux-ia64
                        ;;
                *)
                        OS_TYPE=linux-x86
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/linux" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/linux
                        else
                                JAVA_HOME=$PERF_TOP/../jre/linux
                        fi
                        PERF_LIB_TYPE=linux-x86
                        ;;
                esac
                ;;
    SunOS)
                case `uname -m` in
                sun4*)
               	        OS_TYPE=sol10_64
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE
                        else
                                JAVA_HOME=$PERF_TOP/../jre/$OS_TYPE
                        fi
                        PERF_LIB_TYPE=sol10_64
                        ;;
                *)
                        OS_TYPE=solaris-x86_64
                        if [ -d "$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE" ]; then
                                JAVA_HOME=$PERF_TOP/../jre/$EGO_VERSION/$OS_TYPE
                        else
                                JAVA_HOME=$PERF_TOP/../jre/$OS_TYPE
                        fi
                        PERF_LIB_TYPE=x86-64-sol10
			;;
                esac
                ;;
    esac
else
    case `uname` in
    Linux)
                case `uname -m` in
                ppc64)
                        OS_TYPE=linux-ppc64
                        PERF_LIB_TYPE=linux-ppc64
                        ;;
                x86_64)
                        OS_TYPE=linux-x86_64
                        PERF_LIB_TYPE=linux-x86_64
                        ;;
                ia64)
                        OS_TYPE=linux-ia64
                        PERF_LIB_TYPE=linux-ia64
                        ;;
                *)
                        OS_TYPE=linux-x86
                        PERF_LIB_TYPE=linux-x86
                        ;;
                esac
                ;;
    SunOS)
                case `uname -m` in
                sun4*)
                        OS_TYPE=sol10_64
                        PERF_LIB_TYPE=sol10_64
                        ;;
                *)
                        OS_TYPE=solaris-x86_64
                        PERF_LIB_TYPE=x86-64-sol10
                        ;;
                esac
                ;;
    esac
fi
PATH=${PERF_TOP}/${PERF_VERSION}/bin:${PATH}

case `uname` in
Linux)
    JAVA_HOME_BIN=$JAVA_HOME/bin
    ;;
SunOS)
    if [ "$BINARY_TYPE" = "sparc-sol10-64" ]; then
    	JAVA_HOME_BIN=$JAVA_HOME/bin/sparcv9
    else
    	JAVA_HOME_BIN=$JAVA_HOME/bin/amd64
    fi
    ;;
esac

# reset PERF_LIB
PERF_LIB=""
export PERF_LIB

for file in `find ${PERF_CONFDIR} -name "profile.*.perf" -print`;
do
	if [ ${file} != ${PERF_CONFDIR}/profile.perf ]; then
		. ${file}
	fi
done

export PATH PERF_ENV PERF_TOP PERF_CONFDIR PERF_VERSION OS_TYPE PERF_WORKDIR PERF_LOGDIR PERF_DATADIR PERF_LIB_TYPE 

# add PERF LD_LIBRARY_PATH
echo "${LD_LIBRARY_PATH}" | grep "${PERF_TOP}/ego/${EGO_VERSION}/${PERF_LIB_TYPE}/lib" > /dev/null 2>&1
if [ $? -ne 0 ]; then
    LD_LIBRARY_PATH=$PERF_LIB:$LD_LIBRARY_PATH
fi

export LD_LIBRARY_PATH 
