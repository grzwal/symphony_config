#!/bin/csh
#--------------------------------------------------------------------------
# This code gets the binary type when setting EGO user environment.
#---------------------------------------------------------------------------

set _CUR_PATH_ENV = "$PATH"
setenv PATH /usr/bin:/bin:/usr/local/bin:/local/bin:/sbin:/usr/sbin:/usr/ucb:/usr/sbin:/usr/bsd:${PATH}

# handle difference between system V and BSD echo
# To echo "foo" with no newline, do
# echo $enf "foo" $enl

if ( "`echo -n`" == "-n" )  then
    set enf=
    set enl="\c"
else
    set enf=-n
    set enl=
endif

#---------------Start BINARY_TYPE detection--------------
set LSNULFILE=/dev/null

# Find a version of awk we can trust

set echo_on = `set | grep ^echo | grep -v ^echo_`
if ( "$echo_on" == "echo" ) then
    unset echo
endif

set AWK=""
foreach tmp (awk  /usr/toolbox/nawk gawk nawk)
#   This is the real test, for functions & tolower
#    if foo=`(echo FOO | $tmp 'function tl(str) { return tolower(str) } { print tl($1) }') 2>$LSNULFILE` \
#       && test "$foo" = "foo"
#   A simpler test, just for executability
    set val_tmp=`$tmp 'BEGIN{ } { }'< $LSNULFILE |& cat`
    if ( "$val_tmp" == "" )  then
	set AWK=$tmp
        break
    endif
end

if ( "$echo_on" == "echo" ) then
    set echo
endif
unset echo_on

if (  "$AWK" == "") then
    echo "Cannot find a correct version of awk."
    echo "Exiting ..."
    exit 1
endif
  
   set uname_val=`uname -a`
   set BINARY_TYPE="fail"
   set field1=`echo $uname_val |$AWK '{print $1}'`

   switch ($field1) 
    case Linux:
        set version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
        #get glibc version
	# modified to work without ldconfig
	if ( -x "/bin/sh" && -x "/usr/bin/ldd" ) then
	  set LIBC_SO=`/usr/bin/ldd /bin/sh | grep libc.so.6 | sed 's/.*=>//; s/(.*//'`
	else
          set LIBC_SO=`ldconfig -p | grep libc.so.6 | sed 's/.*=>//'`
	endif
        foreach glibc ( $LIBC_SO )
            if ( -x "$glibc" ) then
                $glibc >& /dev/null
                if ( "$status" == "0" ) then
                    set _libcver=`$glibc | grep "GNU C Library" | awk '{print substr($0,match($0,/version/))}' | awk '{print $2}' | awk -F. '{print $2}'| sed 's/,//'`
                    if ( "$_libcver" != "" ) then
                        break
                    endif
                endif
            endif
        end

        if ( "$_libcver" == "" ) then
            echo "Cannot figure out the GLibc version."
            exit 1
        endif

        if ( $version == "1" ) then
	   set BINARY_TYPE="linux"
	endif
        if (  $version == "2" ) then
            set subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]"."a[2]}'`

            if ( $subver == "2.0" ) then
                set BINARY_TYPE="linux2"
                set machine=`echo $uname_val | $AWK '{print $11}'`
                if ( "$machine" == "alpha" ) then
                    set BINARY_TYPE="linux2-alpha"
                else
                    set BINARY_TYPE="linux2-intel"
                endif
            endif

            if ( $subver == "2.2" ) then
                set BINARY_TYPE="linux2.2"
    
                set smp=`echo $uname_val | $AWK '{print $5}'`
                if ( "$smp" == "SMP" ) then
                    set machine=`echo $uname_val | $AWK '{print $12}'`
                else
                    set machine=`echo $uname_val | $AWK '{print $11}'`
                endif

                if ( "$machine" == "alpha" ) then
		    set BINARY_TYPE="linux2.2-glibc2.1-alpha"
                else
                    if ( "$machine" == "sparc" || "$machine" == "sparc64" ) then
		        set BINARY_TYPE="linux2.2-glibc2.1-sparc"
		    else if ( "$machine" == "ppc" ) then
		        set BINARY_TYPE="linux2.2-glibc2.1-powerpc"
                    else
                        set BINARY_TYPE="linux2.2-glibc2.1-x86"
	            endif		 
                endif
            endif

            if ( $subver == "2.4" ) then
               set BINARY_TYPE="linux2.4"

# ia64 is the family of 64-bit CPUs from Intel. We shouldn't need a new
# distribution for each processor
               set smp=`echo $uname_val | $AWK '{print $5}'`
               if ( "$smp" == "SMP" ) then
                    set machine=`echo $uname_val | $AWK '{print $12}'`
               else
                    set machine=`echo $uname_val | $AWK '{print $11}'`
               endif
          
               if ( "$machine" == "ia64" ) then
		   ls /lib/libc-* >& $LSNULFILE
		   if ( "$status" == "0" ) then
	               set _libcver=`ls /lib/libc-*`
		   else
	               set _libcver=""
		   endif
                   set _libcver=`echo "$_libcver" | $AWK '{split($1, a, "."); print a[2]}'`
		   if ( "$_libcver" == "1" ) then
		       set BINARY_TYPE="linux2.4-glibc2.1-ia64"
		   else if ( "$_libcver" == "2" ) then
	               set BINARY_TYPE="linux2.4-glibc2.2-ia64"
		   else
		       set BINARY_TYPE="linux2.4-glibc2.3-ia64"
		   endif

               else if ( "$machine" == "alpha" ) then
                    set BINARY_TYPE="linux2.4-glibc2.2-alpha"
               else if ( "$machine" == "ppc64" ) then
                    set BINARY_TYPE="linux2.4-glibc2.2-ppc64"
               else if ( "$machine" == "s390" ) then
                    set BINARY_TYPE="linux2.4-glibc2.2-s390-32"
               else if ( "$machine" == "s390x" ) then
                    set BINARY_TYPE="linux2.4-glibc2.2-s390x-64"
               else if ( "$machine" == "armv5l" ) then
		    set BINARY_TYPE="linux2.4-glibc2.2-armv5l"
               else if ( "$machine" == "x86_64" ) then
                      set _cputype=`cat /proc/cpuinfo | grep -i vendor | $AWK '{print $3}' | uniq`
                      if ( "$_libcver" == "1" ) then
                          set BINARY_TYPE="linux2.4-glibc2.1-x86_64"
                      else if ( "$_libcver" == "2" ) then
                          set BINARY_TYPE="linux2.4-glibc2.2-x86_64"
                      else
                          set BINARY_TYPE="linux2.4-glibc2.3-x86_64"
                      endif
               else 
                   set BINARY_TYPE="fail"
                   if ( "$_libcver" == "1" ) then
                       set BINARY_TYPE="linux2.4-glibc2.1-x86"
                   else if ( "$_libcver" == "2" ) then
                       set BINARY_TYPE="linux2.4-glibc2.2-x86"
                   else
                       set BINARY_TYPE="linux2.4-glibc2.3-x86"
                   endif 
               endif
            endif
            if ( $subver == "2.6" ) then
               set BINARY_TYPE="fail"
               set machine=`uname -m`
               
               switch ($machine)
                   case ia64:
                       if ( "$_libcver" == "1" ) then
                           set BINARY_TYPE="linux2.6-glibc2.1-ia64"
                       else if ( "$_libcver" == "2" ) then
                           set BINARY_TYPE="linux2.6-glibc2.2-ia64"
                       else if ( "$_libcver" == "3" ) then
                           set BINARY_TYPE="linux2.6-glibc2.3-ia64"
                       endif
                       breaksw
                   case x86_64:
                       set _cputype=`cat /proc/cpuinfo | grep -i vendor | $AWK '{print $3}' | uniq`
                       if ( "$_libcver" == "1" ) then
                          set BINARY_TYPE="linux2.6-glibc2.1-x86_64"
                       else if ( "$_libcver" == "2" ) then
                          set BINARY_TYPE="linux2.6-glibc2.2-x86_64"
                       else
                          set BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                       endif
                       breaksw
                   case k1om:
                       set _cputype=`cat /proc/cpuinfo | grep -i vendor | $AWK '{print $3}' | uniq`
                       set BINARY_TYPE="linux2.6-glibc2.14-k1om"
                       breaksw
                   case ppc64:    
                       set _cputype=`cat /proc/cpuinfo | grep -i cpu | $AWK '{print $3,$4,$5}' | $AWK -F, '{print $1}' | uniq`
                       if ( "$_libcver" == "5" && "$_cputype" == "Cell Broadband Engine" ) then
                           set BINARY_TYPE="linux2.6-glibc2.5-ppc64_CellBE"
                       else if ( "$_libcver" >= "5" ) then
                           set BINARY_TYPE="linux2.6-glibc2.5-ppc64"
                       endif
                       breaksw    
                   case i[3456]86: 
                       if ( "$_libcver" == "1" ) then
                           set BINARY_TYPE="linux2.6-glibc2.1-x86"
                       else if ( "$_libcver" == "2" ) then
                           set BINARY_TYPE="linux2.6-glibc2.2-x86"
                       else
                           set BINARY_TYPE="linux2.6-glibc2.3-x86"
                       endif
                       breaksw
               endsw
            endif
        endif
        if (  $version == "3" ) then
            set subver=`echo $uname_val | $AWK '{split($3, a, "."); print a[0]}'`
            if ( $subver >= "0" ) then
               set BINARY_TYPE="fail"
               set machine=`uname -m`
               switch ($machine)
                   case x86_64:
                       if ( "$_libcver" >= "3") then
                           set BINARY_TYPE="linux2.6-glibc2.3-x86_64"
                       endif
                       breaksw
                   case ppc64:
                       set BINARY_TYPE="linux2.6-glibc2.5-ppc64"
                       breaksw
               endsw
            endif
        endif
        breaksw
    case SunOS:
        set version=`echo $uname_val | $AWK '{split($3, a, "."); print a[1]}'`
        set minorver=`echo $uname_val | $AWK '{split($3, a, "."); print a[2]}'`
        set machine=`echo $uname_val | $AWK '{print $5}'`
        if ( $version == "4" ) then
	    set BINARY_TYPE="sunos4"
 	else
            if ( "$machine" == "i86pc" ) then
               set BIT64=`/usr/bin/isainfo -vk | grep -c '64.bit'`

               if ( "$BIT64" == "0" ) then
 
                   if ( "$minorver" == "7" || "$minorver" == "8" || "$minorver" == "9" ) then
                       set BINARY_TYPE="x86-sol7"
                   else
                       if ( "$minorver" >= "10" ) then
                           set BINARY_TYPE="x86-sol10"
                       else
                           set BINARY_TYPE="x86-sol2"
                       endif
                   endif
               else
                   if ("$minorver" >= "10") then
                       set BINARY_TYPE="x86-64-sol10"
                   endif
               endif
            else
               set BINARY_TYPE="sparc-sol2"
               if ( "$minorver" >= "7" && "$minorver" <= "9" ) then
     		    set BIT64=`/usr/bin/isainfo -vk | grep -c '64.bit'`
		
	       	    if ( "$BIT64" == "0" ) then
			set BINARY_TYPE="sparc-sol8-32"
		    else
			set BINARY_TYPE="sparc-sol8-64"
                    endif
               else if ( "$minorver" >= "10" ) then
                   set BIT64=`/usr/bin/isainfo -vk | grep -c '64.bit'`

                   if ( "$BIT64" == "0" ) then
                       set BINARY_TYPE="sparc-sol10-32"
                   else
                       set BINARY_TYPE="sparc-sol10-64"
                   endif
                endif
            endif
 	endif
	breaksw
    case AIX:
        set version=`echo $uname_val | $AWK '{print $4}'`
        set release=`echo $uname_val | $AWK '{print $3}'`
        set BIT64=`ls -l /unix |grep -c '64'`
        if ( "$version" == "7" ) then
            if ( "$BIT64" == "0" ) then
                set BINARY_TYPE="aix7-32"
            else
                set BINARY_TYPE="aix7-64"
            endif
        else
            if ( "$BIT64" == "0" ) then
                set BINARY_TYPE="aix5-32"
            else
                set BINARY_TYPE="aix5-64"
            endif
        endif
        breaksw
    default:
        breaksw
    endsw
    if ( $BINARY_TYPE == "fail" ) then
        echo "Cannot get binary type."
    else
    endif

#---------------End of BINARY_TYPE detection--------------

#SOAM Stuff
setenv SOAM_HOME `printenv SOAM_HOME`
setenv SOAM_HOME /opt/ibm/platformsymphony/soam
if (/opt/ibm/platformsymphony/soam == "") then
unsetenv SOAM_HOME
echo "Undefined variable SOAM_HOME. Specify SOAM_HOME in your environment, or in the file cshrc.soam to point to the directory where you installed Symphony Developer Edition. Then, set the environment again: source cshrc.soam."
else
#set BINARY_TYPE=$BINARY_TYPE

setenv SOAM_VERSION 7.1

set ldpath=`printenv LD_LIBRARY_PATH`
 if ( $ldpath == "") then
    setenv LD_LIBRARY_PATH "${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/lib"
 else
    setenv LD_LIBRARY_PATH "${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/lib:$ldpath"
 endif

set hosttype=`echo ${BINARY_TYPE} | cut -d"-" -f3`
 if ( $hosttype == "x86_64" || $hosttype == "k1om" || $BINARY_TYPE == "x86-64-sol10" || $hosttype == "ppc64_CellBE" || $hosttype == "ppc64" || $BINARY_TYPE == "sparc-sol10-64" || $BINARY_TYPE == "aix5-64" ) then
    setenv LD_LIBRARY_PATH "${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/lib64:$LD_LIBRARY_PATH"
 endif

 if ( $BINARY_TYPE == "x86-64-sol10") then
    setenv LD_LIBRARY_PATH "${LD_LIBRARY_PATH}:/lib/64:/usr/ucblib/amd64"
 endif

setenv LIBPATH "$LD_LIBRARY_PATH"

setenv SOAM_BINDIR "${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/bin"

setenv SOAM_SERVERDIR "${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/etc"

setenv BINARY_TYPE ${BINARY_TYPE}
setenv SOAM_BINARY_TYPE ${BINARY_TYPE}

setenv PATH ${SOAM_BINDIR}:${SOAM_SERVERDIR}:${PATH}


#JAVA Stuff
set javahome=`printenv JAVA_HOME`
 if ($javahome != "") then
    setenv PATH $javahome/bin:$PATH
    if (-d "${javahome}/bin/amd64") then
        if ($BINARY_TYPE == "x86-64-sol10") then
            setenv PATH $javahome/bin/amd64:$PATH
        endif
    endif
    if (-d "${javahome}/bin/sparcv9") then
        if ($BINARY_TYPE == "sparc-sol10-64") then
            setenv PATH $javahome/bin/sparcv9:$PATH
        endif
    endif
 endif
 
set anthome=`printenv ANT_HOME`
 if ($anthome != "") then
    setenv PATH $ANT_HOME/bin:$PATH
 endif

  set ostype=`echo $BINARY_TYPE | cut -c 1-8`
  if ($ostype == "linux2.6") then
    set pythonpath=`printenv PYTHONPATH`
    if ($pythonpath != "") then
      setenv PYTHONPATH ${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/lib64:$PYTHONPATH
    else
      setenv PYTHONPATH ${SOAM_HOME}/${SOAM_VERSION}/${BINARY_TYPE}/lib64:
    endif

  endif
endif
